// Dark variant for night mode
export const DARK_BASE = '#373737';
export const DARK_BASE_HIGHLIGHT = '#5A5A5A';
export const DARK_BASE_TEXT = '#BEBEBE';

// Actual App base color theme
export const BASE_COLOR = '#373737';
export const COLOR_WHITE = '#fff';
export const COLOR_DARK = '#797979';
export const COLOR_LIGHT_GREY = '#8c8c8c';
export const COLOR_RED = '#e76e50';
export const COLOR_LIGHTER_GREY = '#E8E8E8';

// Dark theme
export const DARK_THEME = {
  base: DARK_BASE,
  highlight: DARK_BASE_HIGHLIGHT,
  text: DARK_BASE_TEXT,
};

// App Theme
export const APP_THEME = {
  base: BASE_COLOR,
  highlight: DARK_BASE_HIGHLIGHT,
  text: DARK_BASE_TEXT,
};
