import React from 'react';
import { shallow } from 'enzyme';
import TextButton from './TextButton';

describe('Button', () => {
  describe('Rendering', () => {
    it('should match to snapshot', () => {
      const component = shallow(<TextButton label="test label" />);
      expect(component).toMatchSnapshot();
    });
  });
});
